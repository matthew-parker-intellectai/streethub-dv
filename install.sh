# Install Python dependencies
python3 -m pip install -r /home/root/streethub-dv/requirements.txt

# Copy service to systemd directory and change permissions to -rw-r--r--
cp /home/root/streethub-dv/streethub-dv.service /lib/systemd/system/streethub-dv.service
chmod 644 /lib/systemd/system/streethub-dv.service

# Reload systemctl and add new service
sudo systemctl daemon-reload
sudo systemctl enable streethub-dv.service

# Start service
sudo systemctl start streethub-dv.service

# * After updating service file, need to run:
# systemctl daemon-reload
# systemctl restart streethub-dv.service
#
# * To watch python output:
# journalctl -u streethub-dv.service
