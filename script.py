from paho.mqtt.client import Client
from websocket import WebSocket
from datetime import datetime, timedelta
from pathlib import Path
import json
import socket


class StreethubDV(Client):

    # Initialise variables
    def __init__(self):
        Client.__init__(self)
        self.ws = WebSocket()
        self.base_dir = Path(__file__).parent.resolve()
        self.topics = self.get_config(f"{self.base_dir}/topics.json")
        self.streethubs = self.get_config(f"{self.base_dir}/streethubs.json")
        self.chooch_config = self.get_config("/home/root/chooch_ai_on_prem_install/data/config.json")
        self.feeds = {}

    # Run MQTT client
    def run(self):
        self.connect("localhost", 1883)
        self.loop_forever()

    # Run on CONNACK, if successful subscribe to feeds
    def on_connect(self, mqttc, obj, flags, rc):
        if rc == 0:
            print("Connected to MQTT broker")
            self.subscribe_feeds()
        else:
            print(f"MQTT Error. Reason Code: {rc}")

    # On message arrival, parse JSON and check for any valid matches,
    # then for each match (if any), send content trigger to Broadsign server
    def on_message(self, mqttc, obj, msg):
        parsed = json.loads(msg.payload.decode())
        matches = self.find_matches(parsed)
        for (topic, streethub) in matches:
            self.send_trigger(topic, streethub)

    # Subscribe to all camera feeds in ChoochAI config file
    def subscribe_feeds(self):
        for feed in self.chooch_config["camera_feeds"]:
            self.subscribe(f"chooch_capture_detailed_{feed['camera_id']}")
            print(f"Subscribed to feed: {feed['camera_id']}")

    # Checks what classes were identified in message,
    # if any class titles are in our short-listed topics,
    # and if the class score is higher than the min threshold,
    # and if it's been "N+" seconds since the streethub last triggered,
    # then append that class to a list, to be sent to the streethub
    def find_matches(self, data):
        matches = []
        for i in data:
            for topic in self.topics:
                if topic["title"] in i["class_title"] and i["score"] >= topic["min_score"]:
                    streethub = next((s for s in self.streethubs if topic["streethub"] == s["name"]), None)
                    if streethub:
                        current_t = datetime.now()
                        if not streethub["last_t"] or current_t >= streethub["last_t"] + timedelta(seconds=10):
                            streethub["last_t"] = current_t
                            matches.append((topic, streethub))
        return matches

    # Connect to the streethub's Broadsign server over websocket,
    # then generate and send content trigger to server
    def send_trigger(self, topic, streethub):
        print(f"{'-' * 50}\nSending '{topic['title']}' to Broadsign on '{streethub['name']}' ...")
        self.ws.connect(streethub["url"])
        self.ws.send(self.create_trigger(topic))
        response = json.loads(self.ws.recv())
        status = 'Content Accepted' if response['rc']['status'] == '1' else 'Error'
        self.ws.close()
        print(f"Broadsign Response: {status}!\n{'-' * 50}")

    # Generate JSON trigger to send to Broadsign server
    def create_trigger(self, topic):
        return json.dumps({
            "rc": {
                "id": "1",
                "version": "6",
                "action": "trigger",
                "trigger_category_id": f"{topic['code']}",
                "pre_buffer": "0",
                "disable_audio": "0",
                "truncate_current": "0",
                "synchronization_set": "0",
                "synchronization_role": "2",
            }
        })

    # Returns parsed JSON config from file
    def get_config(self, path):
        return json.loads(Path(path).read_text())


# Main function
if __name__ == '__main__':
    try:
        StreethubDV().run()
    except KeyboardInterrupt:
        print(" Keyboard Interrupt: Exiting.")
    except (socket.timeout, socket.error) as error:
        print(f"Socket Error: {error}")
